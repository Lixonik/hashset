import java.util.AbstractSet;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

public class hashSet<E> extends AbstractSet<E> implements Set<E> {

    private HashMap<E, Object> map;

    // Фиктивное значение, нужное для хранения пар ключ-значение в хеш-таблице
    private static final Object DUMMY_VALUE = new Object();

    public hashSet() {
        map = new HashMap<>();
    }

    public hashSet(int initCapacity) {
        map = new HashMap<>(initCapacity);
    }

    public hashSet (int initCapacity, float loadFactor) {
        map = new HashMap<>(initCapacity, loadFactor);
    }

    public boolean add(E element) {
        Object response = map.put(element, DUMMY_VALUE);

        return response == null;
    }

    public boolean contains(Object obj) {
        return map.containsKey(obj);
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    public boolean remove(Object obj) {
        Object response = map.remove(obj);

        return response == DUMMY_VALUE;
    }

    public int size() {
        return map.size();
    }

    public void clear() {
        map.clear();
    }
}
