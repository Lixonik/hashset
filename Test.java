import java.util.Iterator;

public class Test {
    public static void main(String[] args) {
        hashSet<Integer> myHashSet = new hashSet<>();

        for (int i = 0; i < 20; i++) {
            myHashSet.add(i);
        }
        System.out.println(myHashSet);
        // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

        System.out.println(myHashSet.size()); // 20
        System.out.println(myHashSet.isEmpty()); // false
        System.out.println(myHashSet.contains(5)); // true
        System.out.println(myHashSet.contains(666)); // false

        for (int i = -10; i < 11; i++) {
            myHashSet.add(i);
        }
        System.out.println(myHashSet);
        // [0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6, -7, 7, -8, 8, -9, 9, -10, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
        // HashSet хранит элементы в произвольном порядке, но зато быстро ищет.

        for (int i = 0; i < 5; i++) {
            myHashSet.remove(i);
            myHashSet.remove(-i);
        }
        System.out.println(myHashSet);
        // [-5, 5, -6, 6, -7, 7, -8, 8, -9, 9, -10, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

        Iterator<Integer> it = myHashSet.iterator();

        while(it.hasNext()) {
            System.out.print(it.next() + " ");
        }
        System.out.println();
        // -5 5 -6 6 -7 7 -8 8 -9 9 -10 10 11 12 13 14 15 16 17 18 19

        myHashSet.clear();
        System.out.println(myHashSet.isEmpty()); // true
    }
}
